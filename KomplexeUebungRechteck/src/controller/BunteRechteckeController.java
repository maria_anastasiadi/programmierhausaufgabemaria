package controller;
import java.util.LinkedList;
import model.Rechteck;

public class BunteRechteckeController {
	
	private LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>(); // ich gehe davon aus, dass mit "Liste" die LinkedList gemeint ist, da später damit gearbeitet wird

	/*
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	*/
	

	public BunteRechteckeController() {
		rechtecke = new LinkedList<Rechteck>();
	}
	
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void genriereZufallsRechtecke(int anzahl) {
		reset();
		
		for (int i = 0; i < anzahl; i++) {
			add(Rechteck.generiereZufallsRechteck());
		}
	}
	
	

}
