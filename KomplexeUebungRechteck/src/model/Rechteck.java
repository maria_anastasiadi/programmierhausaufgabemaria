package model;
import java.lang.Math;
import java.util.Random;

public class Rechteck {

	//private int x;
	//private int y;
	private Punkt p;
	private int breite;
	private int hoehe;

	public Rechteck () {
		//this.x = 0;
		//this.y = 0;
		this.p = new Punkt(0,0);
		this.breite = 0;
		this.hoehe = 0;
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		this.p = new Punkt(x, y);
		//setX(x);
		//setY(y);
		setBreite(breite);
		setHoehe(hoehe);
	}

	public int getX() {
		return this.p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	public boolean enthaelt(int x, int y) {
		Punkt p = new Punkt(x,y);
		return enthaelt(p);
	}
	
	public boolean enthaelt(Punkt p) {
		int a = p.getX(); 
		int b = p.getY();
		int x = this.getX();
		int y = this.getY();
		int hoehe = this.getHoehe();
		int breite = this.getBreite();
		
		if((a <= x+breite) && (a >= x) && (b <= y+hoehe) && (b >= y)) {
			return true;	
		}
		return false;
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		Punkt p = new Punkt();
		
		p.setX(rechteck.getX());
		p.setY(rechteck.getY());
		if(enthaelt(p) == false) {
			return false;
		}
		
		p.setX(rechteck.getX() + rechteck.getBreite());
		p.setY(rechteck.getY() + rechteck.getHoehe());
		if(enthaelt(p) == false) {
			return false;
		}
		return true;
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Random random = new Random();
		int x = random.nextInt(1200);
		int y = random.nextInt(1000);
		Rechteck zufallRechteck = new Rechteck();
		zufallRechteck.setX(x);
		zufallRechteck.setY(y);
		zufallRechteck.setBreite(random.nextInt(1200-x));
		zufallRechteck.setHoehe(random.nextInt(1000-y));
		
		return zufallRechteck;
	}
	
	
	public String toString() {
		return "Rechteck [x: " + getX() + ", y: " + getY() + ", Breite: " + breite + ", Hoehe: " + hoehe + "]";
	}
	
}
