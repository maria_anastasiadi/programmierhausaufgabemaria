package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;


import controller.BunteRechteckeController;


public class Zeichenflaeche extends JPanel {
	
	private final BunteRechteckeController CONTROLLER;

	public Zeichenflaeche(BunteRechteckeController con) {
		CONTROLLER = con;
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 50, 50);
		
		for (int i = 0; i < CONTROLLER.getRechtecke().size(); i++) {
			g.drawRect(CONTROLLER.getRechtecke().get(i).getX(), CONTROLLER.getRechtecke().get(i).getY(),
					CONTROLLER.getRechtecke().get(i).getBreite(), CONTROLLER.getRechtecke().get(i).getHoehe());
		}
	}
	
}
