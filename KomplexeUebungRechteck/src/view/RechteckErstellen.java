package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RechteckErstellen extends JFrame {

	private JPanel contentPane;
	private JTextField xKoordinateFeld;
	private JTextField yKoordinateFeld;
	private JTextField breiteFeld;
	private JTextField hoeheFeld;
	private int xEingabe = 0;
	private int yEingabe = 0;
	private int breiteEingabe = 0;
	private int hoeheEingabe = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckErstellen frame = new RechteckErstellen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RechteckErstellen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel titelPanel = new JPanel();
		titelPanel.setPreferredSize(new Dimension(10, 100));
		contentPane.add(titelPanel, BorderLayout.NORTH);
		titelPanel.setLayout(new BorderLayout(0, 0));
		
		JLabel titelLabel = new JLabel("Rechteckwunsch");
		titelLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titelLabel.setPreferredSize(new Dimension(46, 50));
		titelPanel.add(titelLabel, BorderLayout.NORTH);
		
		JPanel werteEingabePanel = new JPanel();
		werteEingabePanel.setPreferredSize(new Dimension(10, 5));
		contentPane.add(werteEingabePanel, BorderLayout.CENTER);
		GridBagLayout gbl_werteEingabePanel = new GridBagLayout();
		gbl_werteEingabePanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_werteEingabePanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_werteEingabePanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_werteEingabePanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		werteEingabePanel.setLayout(gbl_werteEingabePanel);
		
		JLabel xKoordinateLabel = new JLabel("xKoordinate");
		GridBagConstraints gbc_xKoordinateLabel = new GridBagConstraints();
		gbc_xKoordinateLabel.insets = new Insets(0, 0, 5, 5);
		gbc_xKoordinateLabel.gridx = 3;
		gbc_xKoordinateLabel.gridy = 2;
		werteEingabePanel.add(xKoordinateLabel, gbc_xKoordinateLabel);
		
		xKoordinateFeld = new JTextField();
		GridBagConstraints gbc_xKoordinateFeld = new GridBagConstraints();
		gbc_xKoordinateFeld.insets = new Insets(0, 0, 5, 5);
		gbc_xKoordinateFeld.fill = GridBagConstraints.HORIZONTAL;
		gbc_xKoordinateFeld.gridx = 3;
		gbc_xKoordinateFeld.gridy = 3;
		werteEingabePanel.add(xKoordinateFeld, gbc_xKoordinateFeld);
		xKoordinateFeld.setColumns(10);
		
		JLabel yKoordinateLabel = new JLabel("yKoordinate");
		GridBagConstraints gbc_yKoordinateLabel = new GridBagConstraints();
		gbc_yKoordinateLabel.insets = new Insets(0, 0, 5, 5);
		gbc_yKoordinateLabel.gridx = 3;
		gbc_yKoordinateLabel.gridy = 5;
		werteEingabePanel.add(yKoordinateLabel, gbc_yKoordinateLabel);
		
		yKoordinateFeld = new JTextField();
		GridBagConstraints gbc_yKoordinateFeld = new GridBagConstraints();
		gbc_yKoordinateFeld.insets = new Insets(0, 0, 5, 5);
		gbc_yKoordinateFeld.fill = GridBagConstraints.HORIZONTAL;
		gbc_yKoordinateFeld.gridx = 3;
		gbc_yKoordinateFeld.gridy = 6;
		werteEingabePanel.add(yKoordinateFeld, gbc_yKoordinateFeld);
		yKoordinateFeld.setColumns(10);
		
		JLabel breiteLabel = new JLabel("breite");
		GridBagConstraints gbc_breiteLabel = new GridBagConstraints();
		gbc_breiteLabel.insets = new Insets(0, 0, 5, 5);
		gbc_breiteLabel.gridx = 3;
		gbc_breiteLabel.gridy = 8;
		werteEingabePanel.add(breiteLabel, gbc_breiteLabel);
		
		breiteFeld = new JTextField();
		GridBagConstraints gbc_breiteFeld = new GridBagConstraints();
		gbc_breiteFeld.insets = new Insets(0, 0, 5, 5);
		gbc_breiteFeld.fill = GridBagConstraints.HORIZONTAL;
		gbc_breiteFeld.gridx = 3;
		gbc_breiteFeld.gridy = 9;
		werteEingabePanel.add(breiteFeld, gbc_breiteFeld);
		breiteFeld.setColumns(10);
		
		JLabel hoeheLabel = new JLabel("hoehe");
		GridBagConstraints gbc_hoeheLabel = new GridBagConstraints();
		gbc_hoeheLabel.insets = new Insets(0, 0, 5, 5);
		gbc_hoeheLabel.gridx = 3;
		gbc_hoeheLabel.gridy = 11;
		werteEingabePanel.add(hoeheLabel, gbc_hoeheLabel);
		
		hoeheFeld = new JTextField();
		GridBagConstraints gbc_hoeheFeld = new GridBagConstraints();
		gbc_hoeheFeld.insets = new Insets(0, 0, 5, 5);
		gbc_hoeheFeld.fill = GridBagConstraints.HORIZONTAL;
		gbc_hoeheFeld.gridx = 3;
		gbc_hoeheFeld.gridy = 12;
		werteEingabePanel.add(hoeheFeld, gbc_hoeheFeld);
		hoeheFeld.setColumns(10);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setPreferredSize(new Dimension(10, 180));
		contentPane.add(buttonPanel, BorderLayout.SOUTH);
		buttonPanel.setLayout(new BorderLayout(0, 0));
		
		JButton rechteckErstellenButton = new JButton("Rechteck erstellen");
		rechteckErstellenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xEingabe = Integer.parseInt(xKoordinateFeld.getText());
				yEingabe = Integer.parseInt(yKoordinateFeld.getText());
				breiteEingabe = Integer.parseInt(breiteFeld.getText());
				hoeheEingabe = Integer.parseInt(hoeheFeld.getText());
				rechteckListen(xEingabe, yEingabe, breiteEingabe, hoeheEingabe);
				
			}
		});
		buttonPanel.add(rechteckErstellenButton, BorderLayout.EAST);
	}
	
	public void rechteckListen (int x,int y,int breite,int hoehe) {
		BunteRechteckeController brc = new BunteRechteckeController();
		brc.add(new Rechteck(x, y, breite, hoehe));
		
	}

}
