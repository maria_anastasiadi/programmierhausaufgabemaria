package test;

import model.Rechteck;
import controller.BunteRechteckeController;

public class RechteckTest {

	public static void main(String[] args) {
		
		// unparametrisierte

		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();

		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);
		
		// parametrisiert
		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		Rechteck rechteck6 = new Rechteck(800,400,20,20);
		Rechteck rechteck7 = new Rechteck(800,450,20,20);
		Rechteck rechteck8 = new Rechteck(850,400,20,20);
		Rechteck rechteck9 = new Rechteck(855,455,25,25);
				
		//Prüfen ob Rechteckerstellung mit negativen Breiten und Höhen möglich	ist
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
		System.out.println(eck10); 
		
		//Rechteck eck11 = new Rechteck();
		Rechteck eck11;
		/*
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		*/
		eck11 = Rechteck.generiereZufallsRechteck();
		System.out.println("Mein Rechteck " + eck11);

		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);
		controller.add(eck10);
		controller.add(eck11);
		
		if(rechteckeTesten() == true) {
			System.out.println("Alle 50000 sind drin");
		} else {
			System.out.println("war wohl nichts");
		}
		
		
		System.out.println(controller.toString());
		System.out.println(controller.toString().equals("BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]"));
	}
	
	//Funktionen
	public static boolean rechteckeTesten() {
		Rechteck rechteck = new Rechteck(0, 0, 1200, 1000);
		Rechteck[] rechteckArray = new Rechteck[50000];
		int c = 0;
		
		for (c = 0; c < 50000; c++) {
			rechteckArray[c] = Rechteck.generiereZufallsRechteck();
			if(rechteck.enthaelt(rechteckArray[c]) == false) {
				return false;
				} 
			}
		return true;
	}
}
